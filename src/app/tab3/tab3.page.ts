import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {DataService} from '../services/data.service';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

    @ViewChild('barCanvas')barCanvas: ElementRef;
    private PieChart: Chart;
    constructor(public dataService: DataService){ }
    public data = [];
    ngOnInit() {
        this.dataService.getTotal().subscribe((response: any[]) => {
            this.data = response;
            console.log(this.data);
        });
    }
    ionViewDidEnter() {
        this.createPieChart();
    }
    createPieChart() {
        // console.log(this.data);
        this.PieChart = new Chart(this.barCanvas.nativeElement, {
            type: 'pie',
            data: {
                labels: ['Total Confirmed', 'Total Deaths', 'Total Recovered'],
                datasets: [{
                    data: [this.data.Global.TotalConfirmed,
                           this.data.Global.TotalDeaths,
                           this.data.Global.TotalRecovered],
                    backgroundColor: [
                        'rgb(0, 0, 255)',
                        'rgb(255, 0, 0)',
                        'rgb(0, 255, 0)'],
                    borderColor: [
                        'rgb(0, 0, 255)',
                        'rgb(255, 0, 0)',
                        'rgb(0, 255, 0)'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

}
