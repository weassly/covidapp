import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {DataService} from '../services/data.service';
import { Chart } from 'chart.js';
import {until} from 'selenium-webdriver';
import elementTextContains = until.elementTextContains;
import {element} from 'protractor';

@Component({

    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
    @ViewChild('barCanvas') barCanvas: ElementRef;
    countryNumber = 0;
    private barChart: Chart;

    constructor(public dataService: DataService) {
    }

    countryname = '';
    favCountries = [];
    public data = [];

    ngOnInit() {
        this.dataService.getTotal().subscribe((response: any[]) => {
            this.data = response;
            console.log(this.data);
        });
    }

    ionChange(event) {
        this.countryname = event.detail.value;
        this.countryLoop();
        console.log(event.detail.value);
    }

    countryLoop() {
        console.log('for loopie');
        // console.log(this.data.Countries.length);
        for (let c = 0; c < this.data.Countries.length; c++) {
            // console.log("Tesess");
            if (this.data.Countries[c].Country === this.countryname) {
                this.countryNumber = c;
                this.createBarChart();
                // console.log("Test!");
            }
        }
    }

    createBarChart() {
        this.barChart = new Chart(this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                labels: ['Total Confirmed', 'Total Deaths', 'Total Recovered'],
                datasets: [{
                    label: 'clear Data',
                    data: [this.data.Countries[this.countryNumber].TotalConfirmed,
                        this.data.Countries[this.countryNumber].TotalDeaths,
                        this.data.Countries[this.countryNumber].TotalRecovered
                    ],
                    backgroundColor: [
                        'rgb(0, 0, 255)',
                        'rgb(255, 0, 0)',
                        'rgb(0, 255, 0)'],
                    borderColor: [
                        'rgb(0, 0, 255)',
                        'rgb(255, 0, 0)',
                        'rgb(0, 255, 0)'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
    setAsFavourite(countryName) {
        if (countryName.length === 0) {
            alert('You did not look for a country!');
        }else {

            if (localStorage.getItem('country')) {

                this.favCountries = JSON.parse(localStorage.getItem('country'));
                if (this.favCountries.includes(countryName)) {
                            alert('You already saved this country!');
                        } else {
                            this.favCountries.push(countryName);
                            localStorage.setItem('country', JSON.stringify(this.favCountries));
                        }
            }
            else {
                this.favCountries.push(countryName);

                localStorage.setItem('country', JSON.stringify(this.favCountries));
            }
        }
    }







}

